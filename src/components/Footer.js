import React from "react";
import {useState} from "react";

import "../styles/Footer.css"

function Footer() {

    const [inputValue, setInputValue] = useState()

    function handleInputValue(event) {
        setInputValue(event.target.value)
    }

    function errorMessage() {
        if(!inputValue.includes("@")) {
            alert("ceci n'est pas un mail valide")          
        }
    }

    return(
        <div>
            <footer className='lmj-footer'>
                <div className='lmj-footer-elem'>
                    Pour les passionné·e·s de plantes 🌿🌱🌵
                </div>

                <div className='lmj-footer-elem'>Laissez-nous votre mail :</div>

                <input 
                    placeholder="entrez votre mail" 
                    type="email" 
                    onChange={handleInputValue} 
                    value={inputValue} 
                    onBlur={errorMessage}
                />
		    </footer>
        </div>
    );
}

export default Footer;