import '../styles/Categories.css'

function Categories({categoriesList, categorySelected, setCategorySelected}) {

     function handleChangeCategory(event) {
        setCategorySelected(event.target.value);    
     }

     function resetCategories() {
         setCategorySelected("");
     }

    return(
        <div className='lmj-categories'> 
            <select 
                value={categorySelected}
                onChange={handleChangeCategory} >
                    <option value="">Catégories des plantes</option>
                    {categoriesList.map( category => (
                        <option 
                            value={category} 
                            key={category} 
                            className="lmj-category" >{category}
                        </option>
                    ))}
            </select>
            <button onClick={resetCategories}>Réinitialiser</button>
        </div>
    );
}

export default Categories;