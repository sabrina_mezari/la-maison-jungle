import {plantList} from "../datas/plantList";
import "../styles/ShoppingList.css";
import PlantItem from "./PlantItem";
import Categories from "./Categories";
import {useState} from "react";


function ShoppingList({cart, updateCart}) {

    const [categorySelected, setCategorySelected] = useState("");

    const categoriesList = plantList.reduce((acc, plant) => 
            acc.includes(plant.category) ? acc : acc.concat(plant.category),
            []
     );

    function addToCart(name, price) {
        const currentPlantAdded = cart.find(plant => plant.name === name);
        if(currentPlantAdded) {
            const cartFilterCurrentPlant = cart.filter(plant => plant.name !== name)
            updateCart([
                ...cartFilterCurrentPlant, 
                {name, price, amount: currentPlantAdded.amount + 1}
            ])
                
        } else {
            updateCart([
                ...cart,
                 {name, price, amount: 1}
            ])
        }
    }
     

    return(
        <div className='lmj-shopping-list'>
            <Categories 
                categoriesList={categoriesList}
                categorySelected={categorySelected}
                setCategorySelected={setCategorySelected}/>
            
            <ul className="lmj-plant-list">
                {plantList.map( ({ name, cover, id, light, water, price, category }) => (
                    !categorySelected || categorySelected === category ? (
                        <div key={id}>
                        <PlantItem 
                            name={name} 
                            cover={cover} 
                            light={light}
                            water={water}
                        /> 
                        <button onClick={() => addToCart(name, price)}>Ajouter</button>
                    </div>

                    ) : null          
                ))}
            </ul> 
        </div>
    );
}

export default ShoppingList;