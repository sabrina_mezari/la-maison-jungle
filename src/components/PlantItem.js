import CareScale from "./CareScale";
import '../styles/PlantItem.css';

function PlantItem({name, cover, id, light, water}) {
    // ou const {name, cover, id, light, water} = props;

    function handleClick(plantName) {
        alert(`vous avez acheté 1 ${plantName} ?, très bon choix`)
    }

    return(
        <div>
            <li key={id} className='lmj-plant-item' onClick={() => handleClick(name)}>
                <img className="lmj-plant-item-cover" src={cover} alt={`${name} cover`}/>
                {name}
                <div>
                    <CareScale careType="light" scaleValue={light}/>
                    <CareScale careType="water" scaleValue={water}/>      
                </div>
            </li>
        </div>
    );
}

export default PlantItem;