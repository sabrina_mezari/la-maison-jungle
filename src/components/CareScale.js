function CareScale({scaleValue, careType}) {

    const range = [1, 2, 3];

    const scaleType = careType === "light" ? '🌞' : "💧"

    const quantityLabel = {
        1: "peu",
        2: "modérément",
        3: "beaucoup"
    }

    function recommandation() {
        alert(`Cette plante requiert ${quantityLabel[scaleValue]} ${ careType === "light" ? "de lumière" : "d'arrosage"}`)
    }

    return(
        <div >
            {range.map( rangeElement =>
                scaleValue >= rangeElement ? <span onClick={recommandation} key={rangeElement.toString() }>{scaleType}</span> : null
            )}
        </div>
    );
}

export default CareScale;